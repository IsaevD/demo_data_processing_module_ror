class AddImageField < ActiveRecord::Migration
  def up
    add_attachment :data_processing_d_example_items, :avatar
  end
  def down
    remove_attachment :data_processing_d_example_items, :avatar
  end

end
