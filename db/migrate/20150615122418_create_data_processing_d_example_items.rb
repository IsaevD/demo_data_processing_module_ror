class CreateDataProcessingDExampleItems < ActiveRecord::Migration
  def change
    create_table :data_processing_d_example_items do |t|
      t.string :name
      t.timestamps
    end
  end
end
