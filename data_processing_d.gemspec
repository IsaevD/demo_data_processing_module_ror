$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "data_processing_d/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "data_processing_d"
  s.version     = DataProcessingD::VERSION
  s.authors     = "Denis Isaev"
  s.email       = "isaevdenismich@mail.ru"
  s.homepage    = "http://www.den-isaev.com"
  s.summary     = "Базовый модуль управления обработкой данных"
  s.description = "Базовый модуль 0-ого уровня для обработки произвольных данных"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.1.0"

  s.add_development_dependency "sqlite3"
end
