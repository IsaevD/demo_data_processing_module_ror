module DataProcessingD
  class ExampleItemsController < ApplicationController

    before_filter :init_variables

    def index
      all_item(@entity, params[:filter], true)
    end

    def show
      get_item_by_id(@entity, params[:id])
    end

    def new
      new_item(@entity)
    end

    def edit
      get_item_by_id(@entity, params[:id])
    end

    def create
      create_item(@entity, @path, prepared_params(@param_name, @params_array))
    end

    def update
      update_item(@entity, @path, params[:id], prepared_params(@param_name, @params_array), @fields)
    end

    def destroys
      delete_item(@entity, @path, params[:id])
    end

    private

      def init_variables
        @entity = ExampleItem
        @param_name = :example_item

        @fields = {
            :name => {
                :type => :string,
                :label => t('form.labels.login'),
                :show_in_table => true
            },
            :avatar => {
                :type => :file,
                :label => t('form.labels.first_name'),
                :show_in_table => false
            }
        }
        @params_array, @field_labels = @fields.map { |k,v| [k.to_s, v.to_s] }.transpose
        @path = data_processing_d.example_items_path
      end

  end
end