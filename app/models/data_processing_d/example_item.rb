module DataProcessingD
  class ExampleItem < ActiveRecord::Base
    has_attached_file :avatar, :styles => { :thumb => "100x100>" }, :default_url => "/images/system_main_items/no_user_avatar.png"
    validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/
  end
end
