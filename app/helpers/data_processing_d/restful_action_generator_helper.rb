module DataProcessingD
  module RestfulActionGeneratorHelper
    include FilterGeneratorHelper

    # Все сущности
    def all_item(entity, filter = nil, is_paginate = true, is_sorted = false)
      paginator_per_page = Rails.configuration.paginator_per_page
      # Использование сортировки
      if (is_sorted)
        if params[:sort_field].nil?
          entity = entity.order(:created_at => :asc)
        else
          entity = entity.order("#{params[:sort_field]} #{params[:sort_value]}")
        end
      end
      # Генерация фильтра, если он определен
      if (!filter.nil?)
        where_statement = generate_where_statement(filter)
        @items = entity.where(where_statement)
      else
        @items = entity.all
      end
      # Использование пэгинации, если она определена
      if (is_paginate)
        @items = @items.paginate(:page => params[:page], :per_page => paginator_per_page)
      end
      return @items
    end

    # Новая сущность
    def new_item(entity)
      return @item = entity.new
    end

    # Получить сущность
    def get_item_by_id(entity, id)
      return @item = entity.find(id)
    end

    # Создать сущность
    def create_item(entity, path, params)
      @item = entity.new(params)
      if @item.save
        redirect_to path
      else
        render "new"
      end
    end

    # Обновить сущность
    def update_item(entity, path, id, params, fields)
      @item = entity.find(id)
      # Предобработка параметров
      params.each do |k, v|
        if fields[k.parameterize.underscore.to_sym][:type] == :file
          if v == "nil"
            params[k] = nil
          end
        end
      end
      if @item.update_attributes(params)
        redirect_to path
      else
        render "new"
      end
    end

    # Удалить сущность
    def delete_item(entity, path, id)
      @item = entity.find(id)
      @item.destroy
      redirect_to path
    end

    # Подготовка параметров
    def prepared_params(param_name, prepared_params)
      params.require(param_name).permit(prepared_params)
    end

  end
end