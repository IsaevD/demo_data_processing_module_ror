module DataProcessingD
  module FilterGeneratorHelper
    def generate_where_statement(data)
      where = []
      # Формирование фильтра по текстовым полям
      if !data[:text].empty?
        data[:text].each do |key, value|
          if value != ""
            where << "#{key} like '%#{value}%'"
          end
        end
      end
      # Формирование фильтра по полям-коллекциям
      if !data[:collection].nil?
        data[:collection].each do |key, value|
          if value != ""
            where << "#{key} = #{value}"
          end
        end
      end
      return where.join(' AND ')
    end
  end
end